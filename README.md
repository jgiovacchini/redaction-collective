Cette forge héberge des textes collectifs.

Merci de n'utiliser que le format markdown pour la rédaction.

Seule la propriétaire de la forge peut créer des branches. Chaque branche est dédiée à un dossier d'écriture. Les collaborateurs doivent intervenir uniquement dans la branche qui les concerne. Les branches sont accessibles par le menu Repository => Branches. 

Dans un dossier tout intervenant peut éditer les fichiers, charger de nouveaux fichiers, fusionner des fichiers.

# Mémo markdown

## paragraphes
Séparer votre texte par des lignes vides pour créer des paragraphes.

Voilà, j'ai créé deux paragraphes.

## emphase

Voici un mot *en italique* à mon sens : je le balise ainsi `*en italique*`

Voici un mot **en gras** : je le balise ainsi : `**en gras**`

## titres

`# titre 1`
`## titre 2`

on peut aussi faire comme ça :

Titre de niveau 1
`=================`
Titre de niveau 2
`-----------------`

## listes à puce

* une puce
* une autre puce
* encore une puce 
   * une sous-puce
        * une sous-sous puce

il suffit d'utiliser un `*` ou un `+` avant chaque entrée et de décaler les sous-puces avec autant de tabulations que nécessaire. Pour des puces numérotées on utilise `1.`, `2.`, etc

1. à la une
2. à la deux
3. à la trois

## citations

> Science sans conscience
> n'est que ruine de l'âme

est une citation un peu dévaluée mais que je peux marquer avec des `>` au début de chaque ligne. 

## liens et images

Rendez-vous sur le  `[Site du Zéro](http://www.siteduzero.com)` pour tout apprendre à partir de Zéro

et une belle image : `![Zozor](http://uploads.siteduzero.com/files/420001_421000/420263.png)`

## ligne de séparation

`---`


